from abc import abstractmethod

from pydantic import BaseModel, Field
from typing import Dict, List

from app.domain.usecases.usecase import Usecase
from app.services.helpers.http import HttpResponse


class Wall(BaseModel):
    height: float
    width: float
    doors: int = 0
    windows: int = 0


class RoomPaintCalculatorParams(BaseModel):
    # Field min_items and max_items secure that every room must be exactly 4 walls
    room: List[Wall] = Field(min_items=4, max_items=4)


class RoomPaintCalculatorResponse(BaseModel):
    total_paint_qty: float
    suggested_tin_sizes: str


class  RoomPaintCalculator(Usecase):
    @abstractmethod
    def execute(self, params:  RoomPaintCalculatorParams) -> HttpResponse:
        raise NotImplementedError()
