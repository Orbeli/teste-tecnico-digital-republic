from app.domain.usecases import Usecase
from app.services.usecases.paint_calculator import RoomPaintCalculator


def room_paint_calculator_factory() -> Usecase:

    return RoomPaintCalculator()
