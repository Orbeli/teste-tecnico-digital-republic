from http import HTTPStatus
from fastapi import Response

from app.main.main import app
from app.main.adapters import fast_api_adapter
from app.main.factories import (
    room_paint_calculator_factory,
)
from app.domain.usecases import (
    RoomPaintCalculatorParams,
    RoomPaintCalculatorResponse,
)

from app.main.routes.helpers import InternalServerError


@app.post(
    '/room_paint_calculator',
    responses={
        HTTPStatus.OK.value: {'model': RoomPaintCalculatorResponse},
        HTTPStatus.INTERNAL_SERVER_ERROR.value: {
            'model': InternalServerError, 'description': 'Internal Server Error'
        },
    },
    status_code=HTTPStatus.NO_CONTENT,
    tags=['Paint Calculator']
)
def room_paint_calculator(body: RoomPaintCalculatorParams, response: Response):
    request = {'body': body, 'headers': None, 'query': None}
    result = fast_api_adapter(request, room_paint_calculator_factory())
    response.status_code = result.status_code
    return result.body
