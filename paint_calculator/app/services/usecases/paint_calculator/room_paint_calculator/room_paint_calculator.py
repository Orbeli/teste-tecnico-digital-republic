
from typing import Dict
from app.domain.usecases import RoomPaintCalculator as RoomPaintCalculatorContract
from app.domain.usecases import RoomPaintCalculatorParams, RoomPaintCalculatorResponse
from app.services.helpers.http import HttpResponse, HttpStatus


class RoomPaintCalculator(RoomPaintCalculatorContract):
    def __init__(
        self,
    ) -> None:
        pass


    def execute(self, params: RoomPaintCalculatorParams) -> HttpResponse:

        for wall in params.room:
            print(wall)

        paint_calculator_response = RoomPaintCalculatorResponse(
            total_paint_qty=18,
            suggested_tin_sizes="18L"
        )

        return HttpStatus.ok_200(
            paint_calculator_response
        )
