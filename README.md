# Teste Técnico Digital Republic
Projeto criado com o objetivo de criar um script, neste caso um endpoint que calcule a quantidade de tinta necessária para pintar uma sala composta por 4 paredes, permitindo que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede. Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L.

---
## Regras de negócio
- Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.
- O total de área das portas e janelas deve ser no máximo 50% da área de parede.
- A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.
- Cada janela possui as medidas: 2,00 x 1,20 metros
- Cada porta possui as medidas: 0,80 x 1,90 metros
- Cada litro de tinta é capaz de pintar 5 metros quadrados.
- Não considerar teto nem piso.
- As variações de tamanho das latas de tinta são: 0,5L;2,5L;3,6L;18L.

---
## Requisitos
1) Python 3.8.x ou superior

---
A instalação e start do projeto pode ser feita com Docker para isso:
1) Rodar o comando:
```
    docker-compose up -d --build
```
2) Pronto, o projeto já está rodando em seu localhost porta 8004, para ver as docs acesse:
```
    http://localhost:8004/docs
```

---
## Start
Após instalado:
1) O projeto pode ser startado novamente através do comando:
```
    docker-compose up
```

---
## Exemplos de Uso
Exemplos de uso com cURL:
1) Para calcular a quantidade de tinta para pintar uma sala e saber o minimo de latas a serem compradas:
```
    curl --location 'http://localhost:8004/room_paint_calculator' \
    --header 'Content-Type: application/json' \
    --data '{
        "room": [
            {
            "height": 5,
            "width": 5,
            "doors": 0,
            "windows": 0
            },
            {
            "height": 5,
            "width": 5,
            "doors": 0,
            "windows": 0
            },
            {
            "height": 5,
            "width": 5,
            "doors": 0,
            "windows": 0
            },
            {
            "height": 5,
            "width": 5,
            "doors": 0,
            "windows": 0
            }
        ]
    }'
```
Este endpoint vai retornar um json informando a quantidade de tinta necessária para pintar a sala e quantas latas de X litros devem ser compradas.

---
## Autor
Gabriel Orbeli Rodrigues Belíssimo

[E-mail](mailto:gabriel.orbeli@gmail.com)
[GitHub](https://github.com/Orbeli)
[Linkedin](https://www.linkedin.com/in/gabriel-orbeli-436815171/)
